<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@ taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt" %>
<jsp:include page="../include/_header.jsp"/>
<h1>PROJECT LIST</h1>

<table width="100%" cellpadding="10" border="1" style="border: black">
    <tr>
        <th width="150" nowrap="nowrap">ID</th>
        <th width="150" nowrap="nowrap" align="left">NAME</th>
        <th width="100%" align="left">DESCRIPTION</th>
        <th width="150" align="center" nowrap="nowrap">STATUS</th>
        <th width="80" align="center" nowrap="nowrap">START</th>
        <th width="80" align="center" nowrap="nowrap">FINISH</th>
        <th width="80" align="nowrap" nowrap="center">EDIT</th>
        <th width="80" align="nowrap" nowrap="center">DELETE</th>
    </tr>

    <c:forEach var="project" items="${projects}">
        <tr>
            <td>
                <c:out value="${project.id}"/>
            </td>
            <td>
                <c:out value="${project.name}"/>
            </td>
            <td>
                <c:out value="${project.description}"/>
            </td>
            <td align="center" nowrap="nowrap">
                <c:out value="${project.status.displayName}"/>
            </td>
            <td align="center">
                <fmt:formatDate pattern="yyyy-MM-dd" value="${project.dateStart}"/>
            </td>
            <td align="center">
                <fmt:formatDate pattern="yyyy-MM-dd" value="${project.dateFinish}"/>
            </td>
            <td align="center">
                <a href="/project/edit/?id=${project.id}">EDIT</a>
            </td>
            <td align="center">
                <a href="/project/delete/?id=${project.id}">DELETE</a>
            </td>
        </tr>
    </c:forEach>
</table>

<form action="/project/create" style="margin-top: 20px;">
    <button style="background-color: white; color: indianred">CREATE PROJECT</button>
</form>

<jsp:include page="../include/_footer.jsp"/>
