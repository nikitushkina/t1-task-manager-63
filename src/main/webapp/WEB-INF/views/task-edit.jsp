<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@ taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt" %>
<jsp:include page="../include/_header.jsp"/>

<h1>TASK EDIT</h1>

<form action="/task/edit?id=${task.id}" method="POST">

    <input type="hidden" name="id" value="${task.id}"/>

    <p>
    <div>NAME:</div>
    <div><input type="text" name="name" value="${task.name}"/></div>
    </p>

    <p>
    <div>DESCRIPTION:</div>
    <div><input type="text" name="description" value="${task.description}"/></div>
    </p>

    <p>
    <div>PROJECT:</div>
    <select name="projectId">
        <option value="">-- // --</option>
        <c:forEach var="project" items="${projects}">
            <option
                    <c:if test="${project.id == task.projectId}">selected="selected"</c:if>
                    value="${project.id}">${project.name}</option>
        </c:forEach>
    </select>
    </p>

    <p>
    <div>STATUS:</div>
    <select name="status">
        <c:forEach var="status" items="${status}">
            <option
                    <c:if test="${task.status == status}">selected="selected"</c:if>
                    value="${status}">${status.displayName}</option>
        </c:forEach>
    </select>
    </p>

    <p>
    <div>DATE START:</div>
    <div><input type="date" name="dateStart"
                value="<fmt:formatDate pattern="yyyy-MM-dd" value="${task.dateStart}" />"/>
    </div>
    </p>

    <p>
    <div>DATE FINISH:</div>
    <div><input type="date" name="dateFinish"
                value="<fmt:formatDate pattern="yyyy-MM-dd" value="${task.dateFinish}" />"/>
    </div>
    </p>

    <button type="submit" style="background-color: white; color: indianred">SAVE TASK</button>

</form>

<jsp:include page="../include/_footer.jsp"/>