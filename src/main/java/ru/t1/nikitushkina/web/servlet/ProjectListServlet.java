package ru.t1.nikitushkina.web.servlet;

import ru.t1.nikitushkina.web.repository.ProjectRepository;

import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;

@WebServlet("/projects/*")
public class ProjectListServlet extends HttpServlet {

    @Override
    protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
        request.setAttribute("projects", ProjectRepository.getInstance().findAll());
        request.getRequestDispatcher("/WEB-INF/views/project-list.jsp").forward(request, response);
    }

}
