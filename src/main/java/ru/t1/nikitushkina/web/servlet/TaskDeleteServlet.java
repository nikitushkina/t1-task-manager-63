package ru.t1.nikitushkina.web.servlet;

import ru.t1.nikitushkina.web.repository.TaskRepository;

import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;

@WebServlet("/task/delete/*")
public class TaskDeleteServlet extends HttpServlet {

    @Override
    protected void doGet(HttpServletRequest request, HttpServletResponse response) throws IOException {
        final String id = request.getParameter("id");
        TaskRepository.getInstance().removeById(id);
        response.sendRedirect("/tasks");
    }

}
