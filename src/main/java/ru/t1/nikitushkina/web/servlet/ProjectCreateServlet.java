package ru.t1.nikitushkina.web.servlet;

import ru.t1.nikitushkina.web.repository.ProjectRepository;

import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;

@WebServlet("/project/create/*")
public class ProjectCreateServlet extends HttpServlet {

    @Override
    protected void doGet(HttpServletRequest request, HttpServletResponse response) throws IOException {
        ProjectRepository.getInstance().create();
        response.sendRedirect("/projects");
    }

}
