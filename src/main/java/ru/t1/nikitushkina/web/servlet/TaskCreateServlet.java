package ru.t1.nikitushkina.web.servlet;

import ru.t1.nikitushkina.web.repository.TaskRepository;

import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;

@WebServlet("/task/create/*")
public class TaskCreateServlet extends HttpServlet {

    @Override
    protected void doGet(HttpServletRequest request, HttpServletResponse response) throws IOException {
        TaskRepository.getInstance().create();
        response.sendRedirect("/tasks");
    }

}
