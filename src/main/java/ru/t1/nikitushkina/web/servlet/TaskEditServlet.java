package ru.t1.nikitushkina.web.servlet;

import lombok.SneakyThrows;
import ru.t1.nikitushkina.web.enumerated.Status;
import ru.t1.nikitushkina.web.model.Task;
import ru.t1.nikitushkina.web.repository.ProjectRepository;
import ru.t1.nikitushkina.web.repository.TaskRepository;

import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;
import java.text.SimpleDateFormat;

@WebServlet("/task/edit/*")
public class TaskEditServlet extends HttpServlet {

    @Override
    protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
        final String id = request.getParameter("id");
        final Task task = TaskRepository.getInstance().findById(id);
        request.setAttribute("task", task);
        request.setAttribute("status", Status.values());
        request.setAttribute("projects", ProjectRepository.getInstance().findAll());
        request.getRequestDispatcher("/WEB-INF/views/task-edit.jsp").forward(request, response);
    }

    @Override
    @SneakyThrows
    protected void doPost(HttpServletRequest request, HttpServletResponse response) {
        final String id = request.getParameter("id");
        final String name = request.getParameter("name");
        final String projectId = request.getParameter("projectId");
        final String description = request.getParameter("description");
        final String statusValue = request.getParameter("status");
        final Status status = Status.valueOf(statusValue);
        final SimpleDateFormat simpleDateFormat = new SimpleDateFormat("yyyy-MM-dd");
        final String dateStartValue = request.getParameter("dateStart");
        final String dateFinishValue = request.getParameter("dateFinish");

        final Task task = new Task();
        task.setId(id);
        task.setName(name);
        task.setProjectId(projectId);
        task.setDescription(description);
        task.setStatus(status);

        if (!dateStartValue.isEmpty()) task.setDateStart(simpleDateFormat.parse(dateStartValue));
        else task.setDateStart(null);
        if (!dateFinishValue.isEmpty()) task.setDateFinish(simpleDateFormat.parse(dateFinishValue));
        else task.setDateStart(null);

        TaskRepository.getInstance().save(task);
        response.sendRedirect("/tasks");
    }

}
