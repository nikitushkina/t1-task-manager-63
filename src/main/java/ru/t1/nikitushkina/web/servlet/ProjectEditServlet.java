package ru.t1.nikitushkina.web.servlet;

import lombok.SneakyThrows;
import ru.t1.nikitushkina.web.enumerated.Status;
import ru.t1.nikitushkina.web.model.Project;
import ru.t1.nikitushkina.web.repository.ProjectRepository;

import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;
import java.text.SimpleDateFormat;

@WebServlet("/project/edit/*")
public class ProjectEditServlet extends HttpServlet {

    @Override
    protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
        final String id = request.getParameter("id");
        final Project project = ProjectRepository.getInstance().findById(id);
        request.setAttribute("project", project);
        request.setAttribute("status", Status.values());
        request.getRequestDispatcher("/WEB-INF/views/project-edit.jsp").forward(request, response);
    }

    @Override
    @SneakyThrows
    protected void doPost(HttpServletRequest request, HttpServletResponse response) {
        final String id = request.getParameter("id");
        final String name = request.getParameter("name");
        final String description = request.getParameter("description");
        final String statusValue = request.getParameter("status");
        final Status status = Status.valueOf(statusValue);
        final SimpleDateFormat simpleDateFormat = new SimpleDateFormat("yyyy-MM-dd");
        final String dateStartValue = request.getParameter("dateStart");
        final String dateFinishValue = request.getParameter("dateFinish");

        final Project project = new Project();
        project.setName(name);
        project.setId(id);
        project.setDescription(description);
        project.setStatus(status);

        if (!dateStartValue.isEmpty()) project.setDateStart(simpleDateFormat.parse(dateStartValue));
        else project.setDateStart(null);
        if (!dateFinishValue.isEmpty()) project.setDateFinish(simpleDateFormat.parse(dateFinishValue));
        else project.setDateStart(null);

        ProjectRepository.getInstance().save(project);
        response.sendRedirect("/projects");
    }

}
