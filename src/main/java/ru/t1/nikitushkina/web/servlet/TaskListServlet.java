package ru.t1.nikitushkina.web.servlet;

import ru.t1.nikitushkina.web.repository.ProjectRepository;
import ru.t1.nikitushkina.web.repository.TaskRepository;

import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;

@WebServlet("/tasks/*")
public class TaskListServlet extends HttpServlet {

    @Override
    protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
        request.setAttribute("tasks", TaskRepository.getInstance().findAll());
        request.setAttribute("projectRepository", ProjectRepository.getInstance());
        request.getRequestDispatcher("/WEB-INF/views/task-list.jsp").forward(request, response);
    }

}
