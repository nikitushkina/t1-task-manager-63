package ru.t1.nikitushkina.web.model;

import lombok.Getter;
import lombok.Setter;
import ru.t1.nikitushkina.web.enumerated.Status;

import java.util.Date;
import java.util.UUID;

@Getter
@Setter
public class Project {

    private String id = UUID.randomUUID().toString();

    private String name;

    private String description;

    private Status status = Status.NOT_STARTED;

    private Date dateStart = new Date();

    private Date dateFinish;

    public Project() {
    }

    public Project(String name) {
        this.name = name;
    }

    public Date getDateStart() {
        return dateStart;
    }

    public void setDateStart(Date dateStart) {
        this.dateStart = dateStart;
    }

    public Date getDateFinish() {
        return dateFinish;
    }

    public void setDateFinish(Date dateFinish) {
        this.dateFinish = dateFinish;
    }

}
