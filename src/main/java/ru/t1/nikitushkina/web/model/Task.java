package ru.t1.nikitushkina.web.model;

import lombok.Getter;
import lombok.Setter;
import ru.t1.nikitushkina.web.enumerated.Status;

import java.util.Date;
import java.util.UUID;

@Getter
@Setter
public class Task {

    private String id = UUID.randomUUID().toString();

    private String name;

    private String description;

    private Status status = Status.NOT_STARTED;

    private Date dateStart = new Date();

    private Date dateFinish;

    private String projectId;

    public Task() {
    }

    public Task(String name) {
        this.name = name;
    }

}
